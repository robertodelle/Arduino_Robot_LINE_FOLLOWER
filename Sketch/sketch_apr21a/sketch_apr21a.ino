// motori alimentati a 7 volt con step up down converter
int speedPinL = 9; // PWM x controllo velocità del motore
int speedPinR = 10; // PWM x controllo velocità del motore

void setup() {
  // Initialize digital output pin
  digitalWrite(2, 0);  // primo stato del pin 2 zero
  pinMode(2,OUTPUT);
  digitalWrite(3, 0);  // primo stato del pin 3 zero
  pinMode(3,OUTPUT);
  digitalWrite(4, 0);  // primo stato del pin 4 zero
  pinMode(4,OUTPUT);
  digitalWrite(5, 0);  // primo stato del pin 5 zero
  pinMode(5,OUTPUT);
  pinMode(speedPinL, OUTPUT);
  pinMode(speedPinR, OUTPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);
}



void loop() {
  // dynamic line follower code
   int lsensor=digitalRead(6);
   int rsensor=digitalRead(7);
   if((lsensor==LOW)&&(rsensor==LOW))
{
//both sensors on white
// go forward
  int speedL = 200;
  int speedR = 200; 
// avanti entrambi i motori
    analogWrite(speedPinL, speedL);//Imposta la velocità via PWM
    analogWrite(speedPinR, speedR);//Imposta la velocità via PWM
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,1);
      digitalWrite(5,0);
}
    
 if((lsensor==HIGH)&& (rsensor==LOW))
{
//left sensor on black line
// turn left 
  int speedL = 0;
  int speedR = 200; 
    // gira a sinistra
     analogWrite(speedPinL, speedL);//Imposta la velocità via PWM
     analogWrite(speedPinR, speedR);
       digitalWrite(2,1); 
       digitalWrite(3,0);
       digitalWrite(4,1);
       digitalWrite(5,0); 
    }
  if((lsensor==LOW)&&(rsensor==HIGH))
{

//right sensor on black line
// turn right
  int speedL = 200;
  int speedR = 0; 
     // gira a destra
    analogWrite(speedPinL, speedL);//Imposta la velocità via PWM
    analogWrite(speedPinR, speedR);//Imposta la velocità via PWM
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,1);
      digitalWrite(5,0);
        }
  if((lsensor==HIGH)&&(rsensor==HIGH))
{

//both sensors on black line
// stop
    int speedL = 0;
    int speedR = 0;  
    analogWrite(speedPinL, speedL);//Imposta la velocità via PWM
    analogWrite(speedPinR, speedR);//Imposta la velocità via PWM
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        }
}

