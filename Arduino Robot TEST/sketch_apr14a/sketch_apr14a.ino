int speedPinA = 9; // Deve essere un pino PWM per essere in grado di controllare la velocità del motore
int speedPinB = 10; // Deve essere un pino PWM per essere in grado di controllare la velocità del motore

void setup() {


  // Initialize digital output pin
  digitalWrite(2, 0);  // primo stato del pin 2 zero
  pinMode(2,OUTPUT);
  digitalWrite(3, 0);  // primo stato del pin 3 zero
  pinMode(3,OUTPUT);
  digitalWrite(4, 0);  // primo stato del pin 4 zero
  pinMode(4,OUTPUT);
  digitalWrite(5, 0);  // primo stato del pin 5 zero
  pinMode(5,OUTPUT);
   pinMode(speedPinA, OUTPUT);
   pinMode(speedPinB, OUTPUT);
  
}



void loop() {
  int speed1 = 50; 
  int speed2 = 100;
// ferma entrambi i motori
    digitalWrite(2,0);
    digitalWrite(3,0);
    digitalWrite(4,0);
    digitalWrite(5,0);
// avanti entrambi i motori speed1
    analogWrite(speedPinA, speed1);//Imposta la velocità via PWM
    analogWrite(speedPinB, speed1);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,1);
      digitalWrite(5,0);
    delay(2000);
    // avanti entrambi i motori speed2
    analogWrite(speedPinA, speed2);//Imposta la velocità via PWM
    analogWrite(speedPinB, speed2);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,1);
      digitalWrite(5,0);
    delay(2000);
// ferma entrambi i motori
    analogWrite(speedPinA, 0);//Imposta la velocità via PWM
    analogWrite(speedPinB, 0);
       digitalWrite(2,0);
       digitalWrite(3,0);
       digitalWrite(4,0);
       digitalWrite(5,0);
    delay(1000);
 // in dietro entrambi i motori
     analogWrite(speedPinA, speed1);//Imposta la velocità via PWM
     analogWrite(speedPinB, speed1);
       digitalWrite(2,0); 
       digitalWrite(3,1);
       digitalWrite(4,0);
       digitalWrite(5,1); 
       delay(2000);
       // ferma entrambi i motori
    analogWrite(speedPinA, 0);//Imposta la velocità via PWM
    analogWrite(speedPinB, 0);
       digitalWrite(2,0);
       digitalWrite(3,0);
       digitalWrite(4,0);
       digitalWrite(5,0);
    delay(1000);
     // gira a destra
    analogWrite(speedPinA, speed1);//Imposta la velocità via PWM
      analogWrite(speedPinB, speed1);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,1);
       delay(1000);
       // ferma entrambi i motori
    analogWrite(speedPinA, 0);//Imposta la velocità via PWM
    analogWrite(speedPinB, 0);
       digitalWrite(2,0);
       digitalWrite(3,0);
       digitalWrite(4,0);
       digitalWrite(5,0);
    delay(1000);
    // gira a sinistra
      analogWrite(speedPinA, speed1);//Imposta la velocità via PWM
     analogWrite(speedPinB, speed1);
       digitalWrite(2,0); 
       digitalWrite(3,1);
       digitalWrite(4,1);
       digitalWrite(5,0); 
        delay(1000);
         // ferma entrambi i motori
    analogWrite(speedPinA, 0);//Imposta la velocità via PWM
    analogWrite(speedPinB, 0);
       digitalWrite(2,0);
       digitalWrite(3,0);
       digitalWrite(4,0);
       digitalWrite(5,0);
    delay(1000);
}

